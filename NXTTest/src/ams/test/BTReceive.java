package ams.test;

import java.io.DataInputStream;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;

public class BTReceive {

	public static void main(String[] args) throws Exception {
		LCD.clear();
		LCD.drawString("Receiver wait...", 0, 0);
		LCD.refresh();

		while (Button.ENTER.isUp()) {
			try {
				LCD.clear(0);
				BTConnection connection = Bluetooth.waitForConnection(10000, 0);
				if (connection != null) {
					DataInputStream input = connection.openDataInputStream();
					LCD.drawString("connected", 0, 3);
					String jsonInput = input.readUTF();
					if (!jsonInput.isEmpty()) {
						LCD.drawString(jsonInput, 0, 2);
					}
					input.close();
					connection.close();
				}
			} catch (Exception ioe) {
				LCD.clear(3);
				LCD.drawString("ERROR", 0, 3);
				LCD.drawString(ioe.getMessage(), 0, 4);
				LCD.refresh();
			}
		}
		try {
			Thread.sleep(4000);
		} catch (Exception e) {
		}
	}
}
