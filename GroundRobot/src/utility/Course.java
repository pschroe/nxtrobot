package utility;

/**
 * Class to carry information about one course (identified by color) and its distance.
 */
public class Course {
	private int color;
	private int distance;
	
	Course(){
		color = -1;
		distance = -1;
	}
	/**
	 * @param color
	 * @param distance
	 */
	public Course(int color, int distance) {
		super();
		this.color = color;
		this.distance = distance;
	}

	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
}
