package utility;

import lejos.nxt.comm.Bluetooth;

/**
 * Infos about the specific robot.
 *
 */
public class RobotInfo {
	
//	/**
//	 * @return Id of the robot
//	 */
//	public int getRobotId(){
//		String name = getRobotName();
//		if(name.equals("Luigi"))
//			return 0;
//		if(name.equals("Mario"))
//			return 1;
//		if(name.equals("Peach"))
//			return 2;
//		return -1;
//	}
//	
	/**
	 * @return Name of the robot
	 */
	public String getRobotName(){
		return Bluetooth.getFriendlyName();
	}
}
