package utility;

import java.util.ArrayList;

/**
 * Stores all explored courses.
 * @author Jonathan Koehn
 */
public class Courses {
	private ArrayList<Course> courseList;
	
	public Courses(){
		courseList = new ArrayList<Course>();
	}
	
	/**
	 * Add course to list.
	 * @param color
	 * @param distance
	 */
	public void addCourse(int color, int distance){
		courseList.add(new Course(color, distance));
	}
	/**
	 * Checks, if course with specific color exists
	 * @param color
	 * @return true, if course exists
	 */
	public boolean courseExists(int color){
		for(Course c : courseList){
			if( c.getColor() == color && c.getDistance() > 0)
				return true;
		}// end for
		return false;
	}
	/**
	 * Checks the list for the shortest course
	 * @return color of the shortest course
	 */
	public int shortestCourse(){
		Course tempShortest = new Course(-1, 5000);
		for(Course c: courseList){
			if(c.getDistance() < tempShortest.getDistance()){
				tempShortest = c;
			}
		}// end for
		return tempShortest.getColor();
	}
}
