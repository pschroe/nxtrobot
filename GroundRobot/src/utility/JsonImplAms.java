package utility;

/**
 * Implements the use of simple and very project specific JSON-Objects as Strings.
 */
public class JsonImplAms {
	/*
	 *  Example calls:
	 	int[] attributes = new int [2];
		attributes[0] = 3;
		attributes[1] = 4;
		int[] noAttributes = new int [0];
		String json = createJson(1,2, attributes);
		LCD.drawString(readJson("s", json), 0, 0);
		LCD.drawString(readJson("t", json), 0, 1);
		LCD.drawString(readJson("a0", json), 0, 2);
		LCD.drawString(readJson("a1", json), 0, 3);
	*/
	
	/**
	 * Returns one attribute of a given String in JSON format
	 * @param choice Name of wanted attribute.
	 * For AMS project usually options for choice are s, t, [a1, [a0]]
	 * @param json Whole JSON-Object
	 * @return Value of wanted attribute
	 */
	public static String readJson(String choice, String json){
		json = json.substring(json.indexOf("\""+choice+"\":")+(choice.length())+5);
		return json.substring(0, 1);
	}
	
	/**
	 * Creats an String in JSON-Syntax for communication between the robots.
	 * @param s Sender
	 * @param t Type
	 * @param att Attributes
	 * @return JSon-Object as String
	 */
	public static String createJson(int s, int t, int [] att){
		String json = "{";
		json += "\"s\": \""+s+"\"";
		json += ",\"t\": \""+t+"\"";
		if(att.length > 0){
			json += ",\"a0\": \""+att[0]+"\"";
			if(att.length > 1)
				json += ", \"a1\": \""+att[1]+"\"";
		}
		json += "}";
		return json;
	}
}
