package utility;

/**
 * For course-specific configuration
 * @author Jonathan Koehn
 *
 */
public class CourseConfig {
	private int blackLineColor = 58;
	private int backgroundColor = 250;
	private int redLineColor = 127;
	private float factor = 0.2f;
	private long finLineTag = 261107616257l;
	
	public int getBlackLineColor() {
		return blackLineColor;
	}
	public int getBackgroundColor() {
		return backgroundColor;
	}
	public int getRedLineColor() {
		return redLineColor;
	}
	public float getFactor() {
		return factor;
	}
	public long getFinLineTag(){
		return finLineTag;
	}
	
}
