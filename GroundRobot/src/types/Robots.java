package types;

/**
 * IDs of the Robots by Names
 * @author Jonathan Koehn
 *
 */
public class Robots {
	public static final int LUIGI = 0;
	public static final int MARIO = 1;
	public static final int PEACH = 2;
}
