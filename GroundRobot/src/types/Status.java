package types;

/**
 * Defining states the robots could be in
 * @author Jonathan Koehn
 *
 */
public class Status {
	public static final int ERROR = -1;
	public static final int EXPLORE = 0;
	public static final int SHORTEST = 1;
	public static final int FASTEST= 2;
}
