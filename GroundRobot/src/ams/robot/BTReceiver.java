/**
 * 
 */
package ams.robot;

import java.io.DataInputStream;
import lejos.nxt.LCD;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;

/**
 * @author Anton Arsenij, Jonathan Koehn, Patrick Schroeder
 *
 */
public class BTReceiver extends Thread {

	/**
	 * method for receiving Input via Bluetooth
	 */
	@Override
	public void run() {
		while (!MainGroundRobot.status()) {
			try {
				BTConnection connection = Bluetooth.waitForConnection(10000, 0);
				if (connection != null) {
					DataInputStream input = connection.openDataInputStream();
					LCD.drawString("connected", 0, 3);
					String jsonInput = input.readUTF();
					if (!jsonInput.isEmpty()) {
//						LCD.drawString(jsonInput, 0, 2);
					}
					input.close();
					connection.close();
				}
			} catch (Exception ioe) {
				LCD.clear(3);
				LCD.drawString("ERROR", 0, 3);
				LCD.refresh();
			}
		}
	}
}
