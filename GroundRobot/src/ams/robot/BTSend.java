/**
 * 
 */
package ams.robot;

import java.io.DataOutputStream;
import java.util.ArrayList;
import javax.bluetooth.RemoteDevice;
import lejos.nxt.LCD;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;

/**
 * @author Anton Arsenij, Jonathan Koehn, Patrick Schroeder
 *
 */
public class BTSend {

	/**
	 * sends massages to all known devices
	 * 
	 * @param input String: message to be sent
	 */
	public static void send(String input) {
		try {
			ArrayList<RemoteDevice> arrayList = new ArrayList<>();
			arrayList = Bluetooth.getKnownDevicesList();
			BTConnection connection = null;
			int i = 0;
			while (i < arrayList.size()) {
				LCD.clear(3);
				RemoteDevice receiver = arrayList.get(i);
				i++;
				if (receiver != null) {
					// LCD.drawString(receiver.getFriendlyName(false), 0, 1);
					connection = Bluetooth.connect(receiver);
				} else {
					LCD.drawString("no reciever", 0, 3);
				}
				if (connection == null) {
					LCD.drawString("no Connection", 0, 3);
				} else {
					LCD.drawString("connected.", 1, 0);
					DataOutputStream output = connection.openDataOutputStream();

					output.writeUTF(input);
					output.flush();
					// LCD.drawString("Sent data", 2, 0);
					output.close();
					connection.close();
					// LCD.drawString("Bye ...", 5, 0);
					LCD.clear();
				}
			}
		} catch (Exception ioe) {
			LCD.clear();
			LCD.drawString("ERROR", 0, 0);
			LCD.refresh();
		}
	}
}
