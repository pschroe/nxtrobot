/**
 * 
 */
package ams.robot;

import lejos.nxt.UltrasonicSensor;

/**
 * @author Anton Arsenij, Jonathan Koehn, Patrick Schroeder
 *
 */
public class DistanceControl extends Thread {

	private Movement movement;

	/**
	 * method for collision avoidance by reducing the speed (proportional speed
	 * reduction)
	 */
	@Override
	public void run() {

		UltrasonicSensor sonicsensor = RobotInit.getUltrasonicSensor();
		int baseSpeed = RobotInit.getBaseSpeed() - 100;// 100 is buffer
		PIDController pid = new PIDController(40, 250);
		final float KP = 12.5f;
		final float KI = 0.000001f;
		final float KD = 10f;
		pid.setPIDParam(PIDController.PID_KP, KP);
		pid.setPIDParam(PIDController.PID_KI, KI);
		pid.setPIDParam(PIDController.PID_KD, KD);
		pid.setPIDParam(PIDController.PID_LIMITHIGH, baseSpeed);
		pid.setPIDParam(PIDController.PID_LIMITLOW, 0);

		while (!MainGroundRobot.status()) {
			sonicsensor.continuous();
			int distance = sonicsensor.getDistance();
			// LCD.clear(5);
			// LCD.drawInt(distance, 0, 5);
			int speedReduction = 0;
			speedReduction = pid.doPID(distance);
			movement.setSpeedReduction(speedReduction);
			// LCD.clear(6);
			// LCD.drawInt(speedReduction, 0, 6);
		}
	}

	/**
	 * setter method
	 * 
	 * @param movement
	 *            the movement to set
	 */
	public void setMovement(Movement movement) {
		this.movement = movement;
	}
}
