/**
 * 
 */
package ams.robot;

import lejos.nxt.Button;
import lejos.nxt.ColorSensor;
import lejos.nxt.LCD;
import lejos.nxt.ColorSensor.Color;

/**
 * @author Anton Arsenij, Jonathan Koehn, Patrick Schroeder
 *
 */
public class MainGroundRobot {

	private static boolean stop = false;

	/**
	 * main method which starts the threads
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		RFIDDetector rfidDetector = new RFIDDetector();
		DistanceControl distanzControl = new DistanceControl();
		BTReceiver btReceiver = new BTReceiver();
		Movement movement = new Movement();
		distanzControl.setMovement(movement);
		LCD.drawString("Ready", 0, 0);
		btReceiver.start();
		distanzControl.start();
		rfidDetector.setMovement(movement);
		rfidDetector.start();
		
//		LCD.drawString("Enter to Start", 0, 1);
//		int button = Button.waitForAnyPress();
//		if(button == Button.ID_ENTER)
			movement.start();
//		if(button == Button.ID_ESCAPE)
//			stop();
	}

	/**
	 * method to set the stop attribute which stops the other threads
	 * 
	 * @param
	 */
	public static void stop() {
		stop = true;
	}

	/**
	 * method to get the running status
	 * 
	 * @param args
	 */
	public static boolean status() {

		return stop;
	}

}
