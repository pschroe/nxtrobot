/**
 * 
 */
package ams.robot;

/**
 * @author Patrick Schroeder
 *
 */
public class PIDController {

	public static final int PID_KP = 0;
	public static final int PID_KI = 1;
	public static final int PID_KD = 2;
	public static final int PID_RAMP_POWER = 3;
	public static final int PID_RAMP_THRESHOLD = 4;
	public static final int PID_DEADBAND = 5;
	public static final int PID_LIMITHIGH = 6;
	public static final int PID_LIMITLOW = 7;
	public static final int PID_SETPOINT = 8;
	public static final int PID_I_LIMITLOW = 9;
	public static final int PID_I_LIMITHIGH = 10;
	public static final int PID_I = 11;
	public static final int PID_PV = 12;

	// Our default Constants for the PID controller
	private float Kp = 1.0f; // proportional value determines the reaction to
								// the current error
	private float Ki = 0.0f; // integral value determines the reaction based on
								// the sum of recent errors
	private float Kd = 0.0f; // derivative value determines the reaction based
								// on the rate at which the error has been
								// changing
	private int highLimit = 900; // assuming control of motor speed and thereby
									// max would be 900 deg/sec
	private int lowLimit = -highLimit;
	private int previous_error = 0;
	private int deadband = 0;
	private int dt = 0; // cycle time, ms
	private long cycleTime = 0; // used to calc the time between each call (dt)
								// to doPID()
	private int setpoint; // The setpoint to strive for
	private int error; // proportional term
	private int integral = 0; // integral term
	private float derivative; // derivitive term
	private int integralHighLimit = 0;
	private int integralLowLimit = 0;
	private boolean integralLimited = false;
	private boolean disableIntegral = false;
	private float power = 0;
	private int rampThresold = 0;
	private double rampExtent = 1;
	private int msdelay;
	private int PV;

	/**
	 * Construct a PID controller instance using passed setpoint (SP) and
	 * millisecond delay (used before returning from a call to doPID()).
	 * 
	 * @param setpoint
	 *            The goal of the MV
	 * @param msdelay
	 *            The delay in milliseconds. Set to 0 to disable any delay.
	 * @see #doPID
	 * @see #setDelay
	 */
	public PIDController(int setpoint, int msdelay) {
		this.setpoint = setpoint;
		this.msdelay = msdelay;
	}

	/**
	 * Set PID controller parameters.
	 * 
	 * @param paramID
	 *            What parameter to set. See the constant definitions for this
	 *            class.
	 * @param value
	 *            The value to set it to. Note that some values are cast to
	 *            <tt>int</tt> depending on the particular <tt>paramID</tt>
	 *            value used.
	 * @see #getPIDParam
	 */
	public void setPIDParam(int paramID, float value) {
		switch (paramID) {
		case PIDController.PID_KP:
			this.Kp = value;
			break;
		case PIDController.PID_KI:
			this.Ki = value;
			break;
		case PIDController.PID_KD:
			this.Kd = value;
			break;
		case PIDController.PID_RAMP_POWER:
			this.power = value;
			rampExtent = Math.pow(this.rampThresold, this.power);
			break;
		case PIDController.PID_RAMP_THRESHOLD:
			this.rampThresold = (int) value;
			if (this.rampThresold == 0)
				break;
			rampExtent = Math.pow(this.rampThresold, this.power);
			break;
		case PIDController.PID_DEADBAND:
			this.deadband = (int) value;
			break;
		case PIDController.PID_LIMITHIGH:
			this.highLimit = (int) value;
			break;
		case PIDController.PID_LIMITLOW:
			this.lowLimit = (int) value;
			break;
		case PIDController.PID_SETPOINT:
			this.setpoint = (int) value;
			this.cycleTime = 0;
			break;
		case PIDController.PID_I_LIMITLOW:
			this.integralLowLimit = (int) value;
			this.integralLimited = (this.integralLowLimit != 0);
			break;
		case PIDController.PID_I_LIMITHIGH:
			this.integralHighLimit = (int) value;
			this.integralLimited = (this.integralHighLimit != 0);
			break;
		default:
			return;
		}
		// zero the Ki accumulator
		integral = 0;
	}

	/**
	 * Get PID controller parameters.
	 * 
	 * @param paramID
	 *            What parameter to get. See the constant definitions for this
	 *            class.
	 * @return The requested parameter value
	 * @see #setPIDParam
	 */
	public float getPIDParam(int paramID) {
		float retval = 0.0f;
		switch (paramID) {
		case PIDController.PID_KP:
			retval = this.Kp;
			break;
		case PIDController.PID_KI:
			retval = this.Ki;
			break;
		case PIDController.PID_KD:
			retval = this.Kd;
			break;
		case PIDController.PID_RAMP_POWER:
			retval = this.power;
			break;
		case PIDController.PID_RAMP_THRESHOLD:
			retval = this.rampThresold;
			break;
		case PIDController.PID_DEADBAND:
			retval = this.deadband;
			break;
		case PIDController.PID_LIMITHIGH:
			retval = this.highLimit;
			break;
		case PIDController.PID_LIMITLOW:
			retval = this.lowLimit;
			break;
		case PIDController.PID_SETPOINT:
			retval = this.setpoint;
			break;
		case PIDController.PID_I_LIMITLOW:
			retval = this.integralLowLimit;
			break;
		case PIDController.PID_I_LIMITHIGH:
			retval = this.integralHighLimit;
			break;
		case PID_I:
			retval = this.integral;
			break;
		case PID_PV:
			retval = this.PV;
			break;
		default:
		}
		return retval;
	}

	/**
	 * Freeze or resume integral accumulation. If frozen, any pre-existing
	 * integral accumulation is still used in the MV calculation. This is useful
	 * for disabling the integral function until the PV has entered the
	 * controllable region [as defined by your process requirements].
	 * <P>
	 * This is one methodology to manage integral windup. This is <tt>false</tt>
	 * by default at instantiation.
	 * 
	 * @param status
	 *            <tt>true</tt> to freeze, <tt>false</tt> to thaw
	 * @see #isIntegralFrozen
	 */
	public void freezeIntegral(boolean status) {
		this.disableIntegral = status;
	}

	/**
	 * 
	 * @return true if the integral accumulation is frozen
	 * @see #freezeIntegral
	 */
	public boolean isIntegralFrozen() {
		return this.disableIntegral;
	}

	/**
	 * Do the PID calc for a single iteration. Your implementation must provide
	 * the delay between calls to this method if you have not set one with
	 * setDelay() or in the constructor.
	 * 
	 * @param processVariable
	 *            The PV value from the process (sensor reading, etc.).
	 * @see #setDelay
	 * @return The Manipulated Variable MV to input into the process (motor
	 *         speed, etc.)
	 */
	public int doPID(int processVariable) {
		int outputMV;
		int delay = 0;
		this.PV = processVariable;

		if (this.cycleTime == 0) {
			this.cycleTime = System.currentTimeMillis();
			return 0;
		}
		error = setpoint - processVariable;
		error = Math.abs(error) <= deadband ? 0 : error;
		if (!disableIntegral)
			integral += Ki * error * dt;
		if (integralLimited) {
			if (integral > integralHighLimit)
				integral = integralHighLimit;
			if (integral < integralLowLimit)
				integral = integralLowLimit;
		}
		derivative = ((float) (error - previous_error)) / dt;
		outputMV = (int) (Kp * error + integral + Kd * derivative);

		if (outputMV > highLimit)
			outputMV = highLimit;
		if (outputMV < lowLimit)
			outputMV = lowLimit;
		previous_error = error;
		outputMV = rampOut(outputMV);

		// delay the difference of desired cycle time and actual cycle time
		if (this.msdelay > 0) {
			delay = this.msdelay - ((int) (System.currentTimeMillis() - this.cycleTime)); // desired
																							// cycle
																							// time
																							// minus
																							// actual
																							// time
			if (delay > 0) {
				try {
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		// global time it took to get back to this statement
		dt = (int) (System.currentTimeMillis() - this.cycleTime);
		this.cycleTime = System.currentTimeMillis();
		return outputMV;
	}

	private int rampOut(int ov) {
		if (power == 0 || rampThresold == 0)
			return ov;
		if (Math.abs(ov) > rampThresold)
			return ov;
		int workingOV;
		workingOV = (int) (Math.pow(Math.abs(ov), power) / rampExtent * rampThresold);
		return (ov < 0) ? -1 * workingOV : workingOV;
	}

	/**
	 * Set the desired delay before doPID() returns. Set to zero to effectively
	 * disable.
	 * 
	 * @param msdelay
	 *            Delay in milliseconds
	 * @see #getDelay
	 */
	public void setDelay(int msdelay) {
		this.msdelay = msdelay;
	}

	/**
	 * Returns the doPID() timing delay.
	 * 
	 * @return The delay set by setDelay()
	 * @see #setDelay
	 */
	public int getDelay() {
		return this.msdelay;
	}
}