/**
 * 
 */
package ams.robot;

import lejos.nxt.LCD;
import lejos.nxt.Sound;
import lejos.nxt.addon.RFIDSensor;
import lejos.robotics.RegulatedMotor;
import types.Status;
import utility.CourseConfig;

/**
 * @author Anton Arsenij, Jonathan Koehn, Patrick Schroeder
 *
 */
public class RFIDDetector extends Thread {
	static int lastRoundDistance;// in cm
	static int newRoundDistance;// in cm
	static int totalTachoCountLeft = 0;
	static int totalTachoCountRight = 0;
	static boolean atGreen = false;
	Movement movement;

	/**
	 * method for determining the lap distance and lap time reset these values
	 * for the next round
	 */
	@Override
	public void run() {
		RFIDSensor rfid = RobotInit.getRfidSensor();
		RegulatedMotor right = RobotInit.getRightMotor();
		RegulatedMotor left = RobotInit.getLeftMotor();
		Sound.setVolume(50);
		CourseConfig cc = new CourseConfig();
		long startId = cc.getFinLineTag();
		long foundId;
		int lastTachoCount = 0;
		int newTachoCount;
		left.resetTachoCount();
		right.resetTachoCount();
		
		while (!MainGroundRobot.status()) {
			foundId = 0;
			foundId = rfid.readTransponderAsLong(true);
			
			if(movement.getState() == Status.EXPLORE){
				if (foundId == startId) {
					newTachoCount = ((right.getTachoCount() + left.getTachoCount()) /2) - lastTachoCount;
					lastTachoCount = newTachoCount;
					Sound.twoBeeps();
					LCD.drawString("U="+ newTachoCount +"      ", 0, 1) ;
					LCD.drawString("d="+ countInDistance(newTachoCount) +"      ", 0, 0) ;
					
							
	/*				int rightcount = right.getTachoCount() - totalTachoCountRight;
					totalTachoCountRight = right.getTachoCount();
					int rightvalue = countInDistance(rightcount);
					int leftcount = left.getTachoCount() - totalTachoCountLeft;
					totalTachoCountLeft = left.getTachoCount();
					int leftvalue = countInDistance(leftcount);
					int addition = rightvalue + leftvalue;
					distance = addition / 2;
					lastRoundDistance = newRoundDistance;
					newRoundDistance = distance;
	*/				
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						LCD.drawString("Error: " + e, 0, 3);
					}
				}// end if(startId)
			} // end if(explore)
		}// end while
	}// end run()

	/**
	 * method for calculating the Distance from Tachocount
	 */
	private int countInDistance(int count) {
		double constant = RobotInit.getWheelDiameter() * Math.PI;
		double value = (count * constant) / 360;
		int result = (int) value;
		return result;
	}// end countInDistance()
	/**
	 * @return Current state of attribute atGreen
	 */
	public boolean getAtGreen(){
		return atGreen;
	}
	public void setMovement(Movement movement){
		this.movement = movement;
	}
}
