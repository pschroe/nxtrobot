/**
 * 
 */
package ams.robot;

import lejos.nxt.ColorSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;
import lejos.nxt.UltrasonicSensor;
import lejos.nxt.addon.RFIDSensor;
import lejos.robotics.RegulatedMotor;

/**
 * @author Patrick Schroeder
 *
 */
public class RobotInit {

	private static final float wheeldiameter = 4.32f;// in cm
	private static final float trackwigth = 16.5f;// in cm
	private static final int basespeed = 500;
	private static final RegulatedMotor leftmotor = Motor.B;
	private static final RegulatedMotor rightmotor = Motor.C;
	private static final UltrasonicSensor ultrasonicsensor = new UltrasonicSensor(SensorPort.S1);
	private static final TouchSensor touchsensor = new TouchSensor(SensorPort.S2);
	private static final ColorSensor colorsensor = new ColorSensor(SensorPort.S3);
	private static final RFIDSensor rfidsensor = new RFIDSensor(SensorPort.S4);

	/**
	 * getter method
	 * 
	 * @return the wheeldiameter
	 */
	public static float getWheelDiameter() {
		return wheeldiameter;
	}

	/**
	 * getter method
	 * 
	 * @return the trackwigth
	 */
	public static float getTrackWigth() {
		return trackwigth;
	}

	/**
	 * getter method
	 * 
	 * @return the basespeed
	 */
	public static int getBaseSpeed() {
		return basespeed;
	}

	/**
	 * getter method
	 * 
	 * @return the leftmotor
	 */
	public static RegulatedMotor getLeftMotor() {
		return leftmotor;
	}

	/**
	 * getter method
	 * 
	 * @return the rightmotor
	 */
	public static RegulatedMotor getRightMotor() {
		return rightmotor;
	}

	/**
	 * getter method
	 * 
	 * @return the ultrasonicsensor
	 */
	public static UltrasonicSensor getUltrasonicSensor() {
		return ultrasonicsensor;
	}

	/**
	 * getter method
	 * 
	 * @return the touchsensor
	 */
	public static TouchSensor getTouchSensor() {
		return touchsensor;
	}

	/**
	 * getter method
	 * 
	 * @return the colorsensor
	 */
	public static ColorSensor getColorSensor() {
		return colorsensor;
	}

	/**
	 * getter method
	 * 
	 * @return the rfidsensor
	 */
	public static RFIDSensor getRfidSensor() {
		return rfidsensor;
	}
}
