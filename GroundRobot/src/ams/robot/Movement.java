/**
 * 
 */
package ams.robot;

import lejos.nxt.Button;
import lejos.nxt.ColorSensor;
import lejos.nxt.LCD;
import lejos.nxt.Sound;
import lejos.robotics.Color;
import lejos.robotics.RegulatedMotor;
import types.Messagetype;
import types.Status;
import utility.CourseConfig;
import utility.Courses;
import utility.RobotInfo;

/**
 * @author Anton Arsenij, Jonathan Koehn, Patrick Schroeder
 *
 */
public class Movement extends Thread {
	static int lightValue;
	static int defaultoffset;	// 128
	static int newoffset;
	static int speedreduction = 0;
	static final float KP = 15f;
	static LCD lcd;
	static final float KI = 0.005f;
	static final float KD = 120f;
	int lineColor = Color.BLACK;
	int state = Status.EXPLORE;
	
	private PIDController initPid(RegulatedMotor left, RegulatedMotor right, int baseSpeed, CourseConfig cc){
		defaultoffset = ((int) ((cc.getBackgroundColor()-cc.getBlackLineColor())*cc.getFactor()));
		PIDController pid = new PIDController(defaultoffset, 10);
		pid.setPIDParam(PIDController.PID_KP, KP);
		// pid.setPIDParam(offset, value); fuer sich aendernden offset
		pid.setPIDParam(PIDController.PID_KI, KI);
		pid.setPIDParam(PIDController.PID_KD, KD);
		pid.setPIDParam(PIDController.PID_LIMITHIGH, left.getMaxSpeed() - baseSpeed);
		pid.setPIDParam(PIDController.PID_LIMITLOW, baseSpeed - left.getMaxSpeed());
		return pid;
	}
	/**
	 * method for driving along a line
	 */
	@Override
	public void run() {
		boolean foundNewBranchThisRound = false;
		int turn;
		CourseConfig cc = new CourseConfig();
		float factor = cc.getFactor();
		Pilot pilot = new Pilot();
		Courses exploredCourses = new Courses();
		ColorSensor light = RobotInit.getColorSensor();
		RegulatedMotor right = RobotInit.getRightMotor();
		RegulatedMotor left = RobotInit.getLeftMotor();
		int baseSpeed = RobotInit.getBaseSpeed();
		PIDController pid = initPid(left, right, baseSpeed, cc);
		right.setSpeed(baseSpeed);
		left.setSpeed(baseSpeed);
		right.forward();
		left.forward();
		LCD.clear();
		Sound.setVolume(50);
		
		while (!Button.ESCAPE.isDown()) {
			lightValue = light.getLightValue();
			turn = pid.doPID( (int) (lightValue*factor));
			//turn = cc.turn(lightValue, turn);
			right.setSpeed(baseSpeed + turn - speedreduction);
			left.setSpeed(baseSpeed - turn - speedreduction);
			
			if(state == Status.EXPLORE){
				// Found red branch
				if(light.getColor().getColor() == Color.RED){
					// Branch is unexplored
					if(! exploredCourses.courseExists(Color.RED)){
						foundNewBranchThisRound = true;
						Sound.beep();
						// senden dass befahren wird
						lineColor = Color.RED;
						exploredCourses.addCourse(Color.RED, Messagetype.EXPLORE);
						defaultoffset = ((int) ((cc.getBackgroundColor()-cc.getRedLineColor())*factor));
						baseSpeed = 200;
						LCD.drawString("red", 0, 4);
						LCD.drawString("explored red", 0, 5);
						//pid = initPid(left, right, baseSpeed, cc);
					}// end if (red, unexplored)
					// branch is explored
	/*				else{
						pilot.forwardAndIgnore(left, right, baseSpeed, 10);
					}
	*/			}//end if(red)
				
				//if(light.getColor().getColor() == Color.GREEN){
	/*			if(rfid.getAtGreen()){
					if(! exploredCourses.courseExists(Color.GREEN)){
						lineColor = Color.GREEN;
						pilot.followBranch(left, right, baseSpeed);
						exploredCourses.addCourse(Color.GREEN, 1);
						defaultoffset = 159;
						LCD.drawString("did green", 0, 6);
						pid = initPid(left, right, baseSpeed);
						left.forward();
						right.forward();
					}
				}// end if(green)
	*/
	
				if((lineColor != Color.BLACK) && (light.getColor().getColor() == Color.BLACK)){
					Sound.buzz();
					baseSpeed = RobotInit.getBaseSpeed();
					lineColor = Color.BLACK;
					defaultoffset = ((int) ((cc.getBackgroundColor()-cc.getBlackLineColor())*cc.getFactor()));
					//pid = initPid(left, right, baseSpeed, cc);
					LCD.drawString("black", 0, 4);
				}// end if(black)
			}// end if(explore)
			
			if(state == Status.SHORTEST){
				
			}// end if(shorest)
		}// end while()
		MainGroundRobot.stop();
		right.stop();
		left.stop();
	}

	/**
	 * setter method. Used by class DistanceControl
	 * 
	 * @param speedReduction
	 *            the speedReduction to set
	 */
	public void setSpeedReduction(int speedReduction) {
		speedreduction = speedReduction;
	}

	/**
	 * setter method
	 * 
	 * @param newOffset
	 *            the newOffset to set
	 */
	public void setNewOffset(int newOffset) {
		newoffset = newOffset;
	}
	/**
	 * Used by RfidDetector
	 * @return
	 */
	public boolean getFoundNewBranchThisRound(){
		return getFoundNewBranchThisRound();
	}
	/**
	 * Used by RfidDetector
	 * @return
	 */
	public int getState(){
		return state;
	}
}
