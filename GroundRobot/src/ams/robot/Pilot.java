package ams.robot;

import lejos.nxt.LCD;
import lejos.nxt.Sound;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.nxt.Sound;

public class Pilot {
	/**
	 * Drive left to follow branch.
	 * 
	 * @param left Left Motor
	 * @param right Right Motor
	 * @param baseSpeed Set base speed
	 */
	public void followBranch(RegulatedMotor left, RegulatedMotor right, int baseSpeed){
		Sound.setVolume(50);
		Sound.beepSequence();
		LCD.drawString("BRANCH", 0, 4);
//		right.setSpeed( (int)(baseSpeed*0.7) );
//		left.setSpeed( (int)(baseSpeed*1.5) );
		DifferentialPilot dpilot = new DifferentialPilot(4.36, 17.2, right, left);
//		dpilot.arc(40, 30);
		dpilot.travelArc(90, 30, false);
//		dpilot.travel(25);
		right.stop();
		left.stop();
		LCD.drawString("            ", 0, 4);
	}
	
	/**
	 * Drives forward to ignore branch.
	 * @param left
	 * @param right
	 * @param baseSpeed
	 * @param distance
	 */
	public void forwardAndIgnore(RegulatedMotor left, RegulatedMotor right, int baseSpeed, int distance){
		DifferentialPilot dpilot = new DifferentialPilot(4.36, 17.2, left, right);
		dpilot.travel(distance);
	}

}
